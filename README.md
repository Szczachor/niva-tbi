Request  : 0xF4, 0x57, 0x01, 0x20, 0x94

Response : 
- 0xF4, 0x97, 0x01,
- 0x19, 0x6D, 0x40, 0x04, 0x00, 0x74, 0x46, 0x11, 0x03, 0x32,
- 0x06, 0x1D, 0x00, 0x00, 0x19, 0x56, 0xFB, 0x78, 0x23, 0x7B,
- 0x06, 0x06, 0x50, 0x00, 0xF4, 0x60, 0x53, 0xB1, 0x50, 0xC7,
- 0x88, 0x94, 0x00, 0x00, 0x00, 0x66, 0x87, 0x70, 0x00, 0xF3,
- 0xA7, 0x00, 0x00, 0x85, 0x00, 0x00, 0xCC, 0x01, 0x20, 0xE9,
- 0x00, 0x81, 0x01, 0x80, 0x6F, 0x00, 0x00, 0x04, 0xA4, 0x09,
- 0x80, 0x0A, 0x44, 0x13, 0x00, 0xCB
 

| Byte | Meaning | Faktor | Offset | Example | Result |
| ------ | ------ | ------ | ------ | ------ | ------ |
||	Initialization (Frame-ID)         | | |	0xF4 | |	
||	Initialization (Length of the string)| | |	0x97 | |				
|| ALDL-mode			| | |	0x01 | |	
||1 (16bit) | Eprom-ID (0x19, 0x6D=Lada 21073, 0x19, 0x63=Lada 21214) | | 0x19, 0x6D | Lada 21073 | 
|3| Error codes 1 (Bit0=error Nr.24, Bit1=23, Bit2=22, Bit3=21, Bit4=15, Bit5=14, Bit6=13, Bit7=12) ||| 0x40	||
|4|	Error codes 2 (Bit0=error Nr.42, Bit1=41, Bit2=35, Bit3=34, Bit4=33, Bit5=32, Bit6=31, Bit7=25) ||| 0x04	||
|5| Error codes 3 (Bit0=error Nr.55, Bit1=54, Bit2=53, Bit3=26, Bit4=51, Bit5=45, Bit6=44, Bit7=43) ||| 0x00 || 	
|6| Coolant temperature |	0,75 |	-40 |	0x74 |	47°C |
|7| Startup Coolant Temp (?) |	0,75 |	-40 |	0x46 |	12,5°C |
|8| Throttle position sensor voltage |	0,019608 |	0 |	0x11 |	0,33V |
|9| Throttle opening in % |0,392157 | 0 | 0x03 | 1,18% |
|10| Engine speed 	| 25 | 0 | 0x32 | 1250 RPM |
|11| Refpulse MSB (?) ||| 0x06 || 	
|12| Refpulse LSB (?) ||| 0x1D || 	
|13| Speed | 1 (1,60934 for km/h) || 0x00 | 0MPH |
|14| unused ||| 0x00 || 	
|15| Lambda probe voltage | 4,44 | 0 | 0x19 | 111mV | 
|16| O2-Sensor cross counts (?) ||| 0x56 || 	
|17|||| 0xFB || 	
|18|||| 0x78 || 	
|19|||| 0x23 || 	
|20|||| 0x7B || 	
|21| IST-Idle regulator position| 1 | 0 | 0x06 | 6 (0 closed, 255 open) |
|22| SOLL-Idle regulator position | 1 | 0 | 0x06 | 6 (0 closed, 255 open) |
|23| Target speed idle | 12,5 | 0 | 0x50 | 1000RPM | 
|24| Probably valve activated carbon filter in % | 0,392157 | 0 | 0x00 | closed | 
|25|||| 0xF4 || 	
|26| MAP (Vacuum at the intake) | 0,37 | 10,34 | 0x60 | 46kPa | 
|27|||| 0x53 || 	
|28|||| 0xB1 ||	
|29| Intake air temperature | 0,75 | -40 | 0x50 | 40°C |
|30|||| 0xC7 || 	
|31| Battery voltage | 0,1 | 0 | 0x88 | 13,6V | 
|32| **(@)Ignition timing adjustment**  | 0,351565 | 0 | 0x94 | 52° (?) |
|33| Ignition timing adjustment (?) ||| 0x00 || 	
|34| Ignition timing adjustment (?) ||| 0x00 || 	
|35 (16bit) | Injection duration | 0,07629 | 0 | 0x000x66 | 7,78ms | 
|37| SOLL-Air/fuel ratio | 0,1 | 0 | 0x87 | 13,5| 
|38|||| 0x70 || 	
|39| Motor running time (is increased by 1 if Bit40 has arrived at 255) | 0 | 255 | 0x00 | 0 | 
|40| Motor running time in seconds | 0 | 1 | 0xF3 | 243 sec. | 
|41|||| 0xA7 || 	
|42|||| 0x00 || 	
|43| unused ||| 0x00 || 	
|44|||| 0x85 || 	
|45|||| 0x00 || 	
|46|||| 0x00 || 	
|47|||| 0xCC || 	
|48|||| 0x01 || 	
|49|||| 0x20 || 	
|50| Buzzword 1 (bit 1 = vehicle is moving, bit 7 = engine is running) ||| 0xE9 | Bit 1=0 stands; Bit 7=1 the engine is running |
|51|||| 0x00 || 	
|52| CLCCMW (Bit 3 = overrun fuel cut-off active, Bit 7 = throttle valve closed) ||| 0x81 | Bit 3 = 0 fuel cut-off not active, bit 7 = 1 throttle valve closed |
|53|||| 0x01 || 	
|54|||| 0x80 || 	
|55|||| 0x6F || 	
|56|||| 0x00 ||	
|57| LCCPMW (Bit 0 motor fan active)||| 0x00 | (Bit 0 = 0 motor fan active)|
|58|||| 0x04 || 	
|59|||| 0xA4 || 	
|60|||| 0x09 || 	
|61|||| 0x80 || 	
|62|||| 0x0A || 	
|63|||| 0x44 ||	
|64|||| 0x13 ||	
|65|||| 0x00 || 	
|66| Checksum ||| 0xCB ||	

**1.7i error codes**

To start flashing the codes, you need to bridge the two extreme, upper right pins in the aldl connector. After starting the procedure, code 12 is always blinked out, informing about the start of diagnostics.

13 no oxygen sensor signal <br /> 
14 coolant temperature (low signal voltage) <br /> 
15 coolant temperature (high signal voltage) <br /> 
21 overvoltage from throttle position sensor <br /> 
22 low voltage from throttle position sensor <br /> 
23 high voltage from air temperature sensor <br /> 
24 no car speed sensor signal <br /> 
25 low voltage from air temperature sensor <br /> 
33 high voltage from atmospheric pressure sensor <br /> 
34 low voltage from atmospheric pressure sensor <br /> 
35 crankshaft sensor error at idle <br /> 
42 ignition control circuit malfunction <br /> 
44 for the poor composition of the mixture <br /> 
45 for the rich composition of the mixture <br /> 
51 EBU calibration board memory error <br /> 
53 high voltage of the power supply <br /> 
54 low or high voltage octane - corrector <br /> 
55 computer error <br /> 

**Current problems and ambiguities**

Byte 32, ignition timing

This byte should indicate the value of the **(@)ignition point** in relation to TDC. It should have values from 0 to 40 ($ 00 to $ 72) and indicates how many degrees before TDC is ignited. We assume a factor of 0.351565 for the calculation. In the lower speed ranges, however, larger angles (sometimes> 40 °) are displayed, which can neither be correct nor agree with values that a TECH1 delivers at this point in time. At higher speeds, the transmitted values are in conceivable orders of magnitude. The two's complement or similar conversions may need to be applied to the hexadecimal value first.

Actuators

The states of all actuators except for the idle speed controller, the fan and probably the valve of the activated carbon filter (lambda probe heating, intake manifold heating, fuel pump) have not yet been identified. A binary transmission in a buzzword is assumed. The active response (switch on / off or set) of all actuators (presumably in mode 4) has also not yet succeeded

References: http://tamas-radelei.de/doku.php/elektrik/steuerung/aldl-protokoll