#include <SoftwareSerial.h>
#include <Wire.h> // Library for I2C communication
#include <LiquidCrystal_I2C.h> // Library for LCD

// Wiring: SDA pin is connected to A4 and SCL pin to A5.
// Connect to LCD via I2C, default address 0x27 (A0-A2 not jumpered)
//LiquidCrystal_I2C lcd = LiquidCrystal_I2C(0x27, 16, 2); // Change to (0x27,20,4) for 20x4 LCD.
LiquidCrystal_I2C lcd = LiquidCrystal_I2C(0x27,20,4);
char LCDmsg[20];

// default SoftwareSerial implementation define size read buffer for 64 bits
// To get larger buffer it has to re-define it in own copy of SoftwareSerial lib 
const int max_size_of_resp = 69;  // 66 bytes for msg; 69 for data stream
const int size_of_req      = 5; 
int count = max_size_of_resp;


char buffer[32]; 
  
struct parameters
{
  uint8_t incalisation[5];
  uint8_t error_codes[3];
  uint8_t coolant_temp;
  uint8_t startup_coolan_temp;
  uint8_t throttle_position;
  uint8_t throttle_opening;
  uint8_t speed_engine;
  uint8_t unused[4];
  uint8_t lambda_voltage;
  uint8_t unused2[5];
  uint8_t IST_position;
  uint8_t SOLL_position;
  uint8_t target_speed_idle;
  uint8_t unused3[7];
  uint8_t battery_volt; 
  uint8_t unused4[5];
  uint8_t air2fuel;
  uint8_t padding[max_size_of_resp-13-5-15];
};


union data_contener
{
  uint8_t    resp[max_size_of_resp]; 
  parameters params;
 };


const byte rxPin = 8;
const byte txPin = 9;

SoftwareSerial Serial1(rxPin, txPin, true);
uint8_t req[5] = {0xF4, 0x57, 0x01, 0x20, 0x94};

unsigned long time1;
unsigned long time2;

int speed_engine(uint8_t speed_engine)
{
  return speed_engine*25; 
}

int target_speed_idle(uint8_t target_speed_idle)
{
  return int(target_speed_idle*12.5F); 
}

float battery_voltage(uint8_t battery_volt)
{
  return (float(battery_volt) * 0.1F); 
}

float lambda_voltage(uint8_t lambda_voltage)
{
  return (float(lambda_voltage) * 4.44F); 
}

float throttle_open(uint8_t throttle_opening)
{
  return (float(throttle_opening) * 0.392157F); 
}

float throttle_position(uint8_t throttle_position)
{
  return (float(throttle_position) * 0.019608F); 
}

float air2fuel_ratio(uint8_t air2fuel)
{
  return (float(air2fuel) * 0.1F);
}

float coolant_temp(uint8_t coolant_temp)
{
  return (float(coolant_temp) * 0.75F - 40);
}

int errors[32] = {0}; 

void code (uint8_t error_codes[], int errors[])
{
  int it = 0; 
  if ((error_codes[0]>>0)&1)
    errors[it++]=24;
  if ((error_codes[0]>>1)&1)
    errors[it++]=23;
  if ((error_codes[0]>>2)&1)
    errors[it++]=22;
  if ((error_codes[0]>>3)&1)
    errors[it++]=21;
  if ((error_codes[0]>>4)&1)
    errors[it++]=15;
  if ((error_codes[0]>>5)&1)
    errors[it++]=14;
  if ((error_codes[0]>>6)&1)
    errors[it++]=13;
  if ((error_codes[0]>>7)&1)
    errors[it++]=12;

  if ((error_codes[1]>>0)&1)
    errors[it++]=42;
  if ((error_codes[1]>>1)&1)
    errors[it++]=41;
  if ((error_codes[1]>>2)&1)
    errors[it++]=35;
  if ((error_codes[1]>>3)&1)
    errors[it++]=34;
  if ((error_codes[1]>>4)&1)
    errors[it++]=33;
  if ((error_codes[1]>>5)&1)
    errors[it++]=32;
  if ((error_codes[1]>>6)&1)
    errors[it++]=31;
  if ((error_codes[1]>>7)&1)
    errors[it++]=25;  

  if ((error_codes[2]>>0)&1)
    errors[it++]=55;
  if ((error_codes[2]>>1)&1)
    errors[it++]=54;
  if ((error_codes[2]>>2)&1)
    errors[it++]=53;
  if ((error_codes[2]>>3)&1)
    errors[it++]=26;
  if ((error_codes[2]>>4)&1)
    errors[it++]=51;
  if ((error_codes[2]>>5)&1)
    errors[it++]=45;
  if ((error_codes[2]>>6)&1)
    errors[it++]=44;
  if ((error_codes[2]>>7)&1)
    errors[it++]=43;  
}

void print_data_on_lcd(struct parameters params)
{
    lcd.setCursor(0, 2);
    lcd.print("TO:");
    lcd.setCursor(3, 2);
    sprintf(LCDmsg, "%2u", int(throttle_open(params.throttle_opening)));
    lcd.print(LCDmsg);

    lcd.setCursor(6, 2);
    lcd.print("T:");
    lcd.setCursor(6+2, 2);
    sprintf(LCDmsg, "%4dV", int(1000*throttle_position(params.throttle_position)));
    lcd.print(LCDmsg);

    lcd.setCursor(7+6, 2);
    lcd.print("L:");
    lcd.setCursor(7+2+6, 2);
    sprintf(LCDmsg, "%3umV", int(lambda_voltage(params.lambda_voltage)));
    lcd.print(LCDmsg);

    lcd.setCursor(0, 3);
    lcd.print("BV:");
    lcd.setCursor(3, 3);
    sprintf(LCDmsg, "%2uV", int(battery_voltage(params.battery_volt)));
    lcd.print(LCDmsg);

    lcd.setCursor(14, 0);
    sprintf(LCDmsg, "I:%d", target_speed_idle(params.target_speed_idle));
    lcd.print(LCDmsg);

    lcd.setCursor(10, 1);
    sprintf(LCDmsg, "I:%2d", int(float(int(params.IST_position))*100/255)); // convert to %
    lcd.print(LCDmsg);
    lcd.setCursor(15, 1);
    sprintf(LCDmsg, "r:%3d", int(10*air2fuel_ratio(params.air2fuel)));
    lcd.print(LCDmsg);
//    lcd.setCursor(18, 1);
//    sprintf(LCDmsg, "%d", params.SOLL_position);
//    lcd.print(LCDmsg);

    code(params.error_codes, errors);
    lcd.setCursor(0, 0);
    lcd.print("E: ");
    int ldc_itr = 0;
    for(int idx = 0; idx < 16; idx++)
    {
        if(0 != errors[idx])
        {
          lcd.setCursor(2+ldc_itr, 0);
          sprintf(LCDmsg, "%u,", errors[idx]);
          lcd.print(LCDmsg);
          ldc_itr = ldc_itr + 3;
        }
    }
    
    lcd.setCursor(0, 1);
    lcd.print("E: ");
    lcd.setCursor(2, 1);
    ldc_itr = 0;
    for(int idx = 16; idx < 32; idx++)
    {
        if(0 != errors[idx])
        {
          lcd.setCursor(2+ldc_itr, 1);
          sprintf(LCDmsg, "%u,", errors[idx]);
          lcd.print(LCDmsg);
          ldc_itr = ldc_itr + 3;
        }
    }

    lcd.setCursor(7, 3);
    lcd.print("S:");
    lcd.setCursor(7+2, 3);
    sprintf(LCDmsg, "%d", int(speed_engine(params.speed_engine)));
    lcd.print(LCDmsg);
    
    lcd.setCursor(7+8, 3);
    lcd.print("C:");
    lcd.setCursor(7+2+8, 3);
    sprintf(LCDmsg, "%3d", int(coolant_temp(params.coolant_temp)));
    lcd.print(LCDmsg);
}

void print_data_on_serial(union data_contener data)
{
    int check_sum = 0;
    
    Serial.print("--------------------------------------\n");
    Serial.print("throttle_open: ");
    Serial.print(throttle_open(data.params.throttle_opening));
    Serial.print("[per]\n");
    
    Serial.print("throttle_position: ");
    Serial.print(throttle_position(data.params.throttle_position));
    Serial.print("[V]\n");

    Serial.print("lambda probe voltage: ");
    Serial.print(lambda_voltage(data.params.lambda_voltage));
    Serial.print("[mV]\n");

    Serial.print("battery voltage: ");
    Serial.print(battery_voltage(data.params.battery_volt));
    Serial.print("[V]\n");
    
    code(data.params.error_codes, errors);
    Serial.print("errors: ");
    for(int idx = 0; idx < 32; idx++)
    {
        Serial.print(errors[idx]);
        Serial.print(" ");
    }
    Serial.print(" \n");

    Serial.print("speed_engine: ");
    Serial.print(speed_engine(data.params.speed_engine));
    Serial.print("[RPM]\n");


      
    Serial.print("--------------------------------------\n");
    for(int idx = 0; idx < max_size_of_resp+2; idx++)
    {
        check_sum += (int)data.resp[idx];
        Serial.print(data.resp[idx], HEX);
        Serial.print(" ");
    }
    Serial.print(data.resp[max_size_of_resp+3], HEX);
    Serial.print(" ");
    Serial.print(" \n");
    check_sum = (256-check_sum);
    Serial.print(check_sum, HEX);
    if(check_sum == data.resp[max_size_of_resp+3])
      Serial.print("CS: OK\n");
    else
      Serial.print("CS: NOK\n");
    Serial.print("--------------------------------------\n");
}

void setup() 
{
  lcd.init();
  lcd.backlight();
  lcd.setCursor(0, 0);
  lcd.print("8  8  8  8   8   8");
  lcd.setCursor(0, 1);
  lcd.print("88 8  8   8 8   8 8");
  lcd.setCursor(0, 2);
  lcd.print("8 88  8   888   888");
  lcd.setCursor(0, 3);
  lcd.print("8  8  8    8    8 8");
  Serial.begin(9600);
  Serial1.begin(8192);

  Serial.println("Start");
  delay(1000);
  lcd.clear();
  data_contener data_null = {0};
  print_data_on_lcd(data_null.params); 
  for(int idx = 0; idx < size_of_req; idx++)
  {
    Serial1.write(req[idx]);
  }
}

void loop() 
{
  data_contener data      = {0};
  uint8_t       temp      = 0; 
  int           resp_it   = 0; 
  uint8_t len             = 0; 
  uint8_t raw_len         = 0;
  bool    is_data_printed = false;   
//  print_data_on_lcd(data.params);   
  
  delay(500);
  for(int idx = 0; idx < size_of_req; idx++)
  {
    Serial1.write(req[idx]);
  }

  while(Serial1.available() > 0) 
  {    
      if(Serial1.read() == 0xF4)  // read 1 byte
      {
        Serial.println(" ");
        Serial.println("premble");

        is_data_printed = true; 
        
        data.resp[resp_it++] = 0xF4;
        data.resp[resp_it++] = Serial1.read(); // len - read 2 byte
        data.resp[resp_it++] = Serial1.read(); // mode - read 3 byte

        Serial.println(data.resp[1]);
        if(data.resp[1] != 0x57)
        {
          len = (data.resp[1] - 0x55);
          for(int idx = resp_it; idx < len + resp_it; idx++)
          {
           data.resp[idx] = Serial1.read(); // read 66 bytes
          }
        }
      }
      else
        break; 
  }

  if(is_data_printed == true)
  {
    print_data_on_lcd(data.params);  
    print_data_on_serial(data);
  }


}
