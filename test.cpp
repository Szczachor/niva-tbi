/******************************************************************************

                            Online C Compiler.
                Code, Compile, Run and Debug C program online.
Write your code in this editor and press "Run" button to compile and execute it.

*******************************************************************************/

#include <stdio.h>
#include <stdint.h>

const int max_size_of_resp = 69;
const int size_of_req      = 5; 


char buffer[32]; 
  
struct parameters
{
  uint8_t incalisation[5];
  uint8_t error_codes[3];
  uint8_t coolant_temp;
  uint8_t startup_coolan_temp;
  uint8_t throttle_position;
  uint8_t throttle_opening;
  uint8_t speed_engine;
  uint8_t padding[max_size_of_resp-13];
};


union data_contener
{
  uint8_t    resp[max_size_of_resp]; 
  parameters params;
 };

void code (uint8_t error_codes[], int errors[])
{
  int it = 0; 
  if ((error_codes[0]>>0)&1)
    errors[it++]=24;
  if ((error_codes[0]>>1)&1)
    errors[it++]=23;
  if ((error_codes[0]>>2)&1)
    errors[it++]=22;
  if ((error_codes[0]>>3)&1)
    errors[it++]=21;
  if ((error_codes[0]>>4)&1)
    errors[it++]=15;
  if ((error_codes[0]>>5)&1)
    errors[it++]=14;
  if ((error_codes[0]>>6)&1)
    errors[it++]=13;
  if ((error_codes[0]>>7)&1)
    errors[it++]=12;
}

float throttle_open(uint8_t throttle_opening)
{
  return (float(throttle_opening) * 0.392157F); 
}

int speed_engine(uint8_t speed_engine)
{
  return speed_engine*25; 
}

int main()
{
    printf("Hello World");
    data_contener  data = {0};
    // data.resp
    uint8_t kot[] = {0xF4, 0x97, 0x01,
0x19, 0x6D, 0x40, 0x04, 0x00, 0x74, 0x46, 0x11, 0x03, 0x32,
0x06, 0x1D, 0x00, 0x00, 0x19, 0x56, 0xFB, 0x78, 0x23, 0x7B,
0x06, 0x06, 0x50, 0x00, 0xF4, 0x60, 0x53, 0xB1, 0x50, 0xC7,
0x88, 0x94, 0x00, 0x00, 0x00, 0x66, 0x87, 0x70, 0x00, 0xF3,
0xA7, 0x00, 0x00, 0x85, 0x00, 0x00, 0xCC, 0x01, 0x20, 0xE9,
0x00, 0x81, 0x01, 0x80, 0x6F, 0x00, 0x00, 0x04, 0xA4, 0x09,
0x80, 0x0A, 0x44, 0x13, 0x00, 0xCB};
    // data.resp = kot;
    for(int idx = 0; idx < max_size_of_resp; idx++)
     data.resp[idx] = kot[idx];
    int errors[32] = {0};
    int length = 0;
    
    printf(" code 0x%x\n", data.resp[11]);
    length += sprintf(buffer + length, "throttle_open: %f [per]\n", throttle_open(data.params.throttle_opening));
    
    
    code(data.params.error_codes, errors);
    printf(" first code 0x%x\n", data.params.error_codes[0]);
    for(int idx = 0; idx < 8; idx++)
    {
        length += sprintf(buffer + length, "errors: %d\n", errors[idx]);
    }
    

    
    length += sprintf(buffer + length, "speed_engine: %d [RPM]\n", speed_engine(data.params.speed_engine));
    printf("%s", buffer);
    return 0;
}
